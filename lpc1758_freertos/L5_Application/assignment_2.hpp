#ifndef ASSIGNMENT2_HPP_
#define ASSIGNMENT2_HPP_

#include "tasks.hpp"
#include "eint.h"
#include "gpio.hpp"
#include "io.hpp"

SemaphoreHandle_t sem = 0;

void switch_edge(void)
{
    xSemaphoreGive(sem);
}

class Transmitter_LESWtask: public scheduler_task
{
    private:
        GPIO p2_wire0;
    public:
        Transmitter_LESWtask(uint8_t priority) :
                scheduler_task("hw_task", 2000, priority), p2_wire0(
                        LPC1758_GPIO_Type::P2_0)
        {
            //Nothing to init
        }

        bool init(void)
        {
            p2_wire0.setAsOutput();
            const uint8_t port2_1 = 1;
            eint3_enable_port2(port2_1, eint_rising_edge, switch_edge);
            LE.init();
            vSemaphoreCreateBinary(sem);
            return true;
        }

        bool run(void *p)
        {
            if (xSemaphoreTake(sem, 9999))
            {
                //p2_wire0.toggle();
                p2_wire0.setHigh();
                vTaskDelay(500);
                p2_wire0.setLow();
            }
            return true;
        }
};

class Receiver_LESWtask: public scheduler_task
{
    private:
        GPIO p2_wire0;
    public:
        Receiver_LESWtask(uint8_t priority) :
                scheduler_task("switch_task", 2000, priority), p2_wire0(
                        LPC1758_GPIO_Type::P2_0)
        {
            // Nothing to init
        }

        bool init(void)
        {
            p2_wire0.setAsInput();
            SW.init();
            LE.init();
            return true;
        }

        bool run(void *p)
        {
            if (p2_wire0.read())
            {
                // Turn on LED1
                LE.on(1);
            }
            else
            {
                // Turn off LED2
                LE.off(1);
            }
            return true;
        }
};

#endif /*ASSIGNMENT2_HPP_*/
